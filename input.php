<!DOCTYPE HTML>  
<html>
<head>
<style>
.error {color: #FF0000;}
</style>
</head>
<body> 
    <?php if (isset($_SESSION['message'])): ?>
	<div class="msg">
		<?php 
			echo $_SESSION['message']; 
			unset($_SESSION['message']);
		?>
	</div>
<?php endif ?>
<!DOCTYPE HTML>  
<html>
<head>
<style>
.error {color: #FF0000;}
</style>
 <link rel="stylesheet" type="text/css" href="style.css">   
</head>
<body>  
    <?php
// define variables and set to empty values
$firstnameErr=$lastnameErr = $emailErr = $genderErr = $contactErr = $dobErr="";
$firstname =$lastname= $email = $gender = $contact =$dob = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["firstname"])) {
    $firstnameErr = "First Name is required";
  } else {
    $firstname = test_input($_POST["firstname"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$firstname=$_POST["firstname"])) {
        $firstname=" ";
      $firstnameErr = "Only letters and white space allowed";
    }
  }
    if (empty($_POST["lastname"])) {
    $lastnameErr = "Second Name is required";
  } else {
    $lastname = test_input($_POST["lastname"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$lastname=$_POST["lastname"])) {
        $lastname=" ";
      $lastnameErr = "Only letters and white space allowed";
    }
  }
  
  if (empty($_POST["email"])) {
    $emailErr = "Email is required";
  } else {
    $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Invalid email format";
    }
  }
    
    if (empty($_POST["contact"])) {
    $contactErr = "Contact is required";
  } else {
    if(!preg_match('/^[254]{3}-[0-9]{10}$/',$contact=$_POST["contact"]))
    {
        $contactErr = "Wrong format";
    }
    }
    
  if (empty($_POST["dob"])) {
    $dobErr = "Date of birth is required";
  } else {
    $dob = test_input($_POST["dob"]);
  }

    if (empty($_POST["gender"])) {
    $genderErr = "Gender required";
  } else {
    $gender = test_input($_POST["gender"]);
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>
<h2>PHP Form Validation</h2>
    <div class="form">
<p><span class="error">* required field</span></p>
<form method="post" action="insert.php".<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>> 
    <div class="input-group">
  First Name: <input type="text" name="firstname" required="required" pattern="[A-Za-z]*$">
  <span class="error">* <?php echo $firstnameErr;?></span>
  <br><br>
        </div>
        <div class="input-group">
  Last Name: <input type="text" name="lastname" required="required" pattern="[A-Za-z]*$">     
  <span class="error">* <?php echo $lastnameErr;?></span>
  <br><br>
        </div>
        <div class="input-group">
  E-mail: <input type="text" name="email" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$">
  <span class="error">* <?php echo $emailErr;?></span>
  <br><br>
        </div>
        <div class="input-group">
  Format: +2547000000000<br>
  Contact: <input type="number" name="contact" patern="[0-9]{10,13}" required="required">
  <span class="error">*<?php echo $contactErr;?></span>
  <br><br>
            </div>
            <div class="input-group">
  Date of birth: <input type="date" name="dob" required="required">
  <span class="error">*<?php echo $dobErr;?></span>
  <br><br>
        </div>
        <div class="input-group">
  Gender:
  <input type="radio" name="gender"  required="required" value="female">Female
  <input type="radio" name="gender" required="required" value="male">Male
  <input type="radio" name="gender" required="required" value="other">Other
  <span class="error">* <?php echo $genderErr;?></span>
  <br><br>
    </div>
        <div class="input-group">
         <input class="btn" type="submit" name="save"/>
    </div>
                     </form>
                     </div>
</body>
</html>